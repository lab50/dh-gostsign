#!/usr/bin/perl

# © ООО «Лаборатория 50», 2019
#
# Условия лицензирования описаны в файле COPYING.

=head1 NAME

dh_astrasign - подпись исполняемых файлов и разделяемых библиотек цифровой подписью

=cut

use strict;
use warnings;
use File::Find;
use Config;
use Debian::Debhelper::Dh_Lib;

=head1 SYNOPSIS

B<dh_astrasign> [S<I<debhelper options>>]

=head1 DESCRIPTION

B<dh_astrasign> это программа для debhelper, которая подписывает ELF-файлы пакетов
цифровой подписью с помощью утилиты bsign.

Подпись должна быть доступна в хранилище GPG пользователя.
С помощью переменной окружения ASTRASIGN можно указать используемую GPG-подпись.

=head1 OPTIONS

=over 4

=item B<--> I<params>

I<params> дополнительные параметры для gpg.

=back

=cut

init();

my @elf_files;

sub _get_file_type($) {
	my $file=shift;
	open(FILE, '-|') # handle all filenames safely
	  || exec('file', '-b', $file)
	  || die "can't exec file: $!";
	my $type=<FILE>;
	close FILE;
	return $type;
}

sub testfile {
	return if -l $_ or -d $_; # Skip directories and symlinks always.

	if (_get_file_type($_) =~ m/ELF [\w\d -]* shared object/) {
		push @elf_files, $File::Find::name;
	}
}

my @pgoptions = ("--batch");
if (defined $ENV{ASTRASIGN} && $ENV{ASTRASIGN} ne "") {
	push @pgoptions, "--default-key=$ENV{ASTRASIGN}"
}
if (defined $dh{U_PARAMS}) {
	push @pgoptions, @{$dh{U_PARAMS}};
}
my $pgparms = join(" ", @pgoptions);

sub my_error_exitcode {
	my $command=shift;
	if ($? == -1) {
		error("$command failed to to execute: $!");
	}
	elsif ($? & 127) {
		error("$command died with signal ".($? & 127));
	}
	elsif ($?) {
		nonquiet_print("$command returned exit code ".($? >> 8));
		return $? >> 8;
	}
	else {
		warning("This tool claimed that $command have failed, but it");
		warning("appears to have returned 0.");
		error("Probably a bug in this tool is hiding the actual problem.");
	}
}

sub my_doit {
	if (doit_noerror(@_)) {
		return 0;
	} else {
		return my_error_exitcode(join(" ", @_));
	}
}

sub dosign($) {
	my $file=shift;
	if (defined $ENV{SOURCE_DATE_EPOCH}) {
		return my_doit("faketime", "\@$ENV{SOURCE_DATE_EPOCH}", "bsign", $dh{VERBOSE} ? "-ENs" : "-qENs", "--pgoptions=$pgparms", $file);
	} else {
		return my_doit("bsign", $dh{VERBOSE} ? "-ENs" : "-qENs", "--pgoptions=$pgparms", $file);
	}
}

foreach my $package (@{$dh{DOPACKAGES}}) {
	next if is_udeb($package);
	next if package_arch($package) eq 'all';
	next if $package =~ /-dbg$/;
	next if $package =~ /-dbgsum$/;

	@elf_files = ();
	my $tmp=tmpdir($package);
	find(\&testfile, $tmp);
	next unless @elf_files;

	foreach (@elf_files) {
		my $file = basename($_);
		my $rc = 1;
		for (my $i = 0; $i < 10 && $rc != 0; $i++) {
			nonquiet_print("signing $file");
			$rc = dosign($_);
			if ($rc != 0 && $rc != 69) {
				error("bsign $file failed");
			} else {
				last;
			}
		}
	}
}
