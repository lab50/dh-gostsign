# DH Sequence

# © ООО «Лаборатория 50», 2019

use warnings;
use strict;
use Debian::Debhelper::Dh_Lib;

insert_after('dh_strip', 'dh_astrasign');

1;
